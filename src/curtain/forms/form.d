module curtain.forms.form;

import curtain.forms.field;
import std.conv;
import std.meta;
import std.string;
import std.traits;
import vibe.inet.webform;


/**
	Basic class for forms.
	
	When creating a custom form, you should always derive from this class
	and declare one or more members with Field-based UDAs, e.g.:
	
		class LoginForm: Form {
			@TextField string text;
			@TextField string password;
		}
	
	After form is instantiated, you can use various functions from this module to work with it.
	None of them are real Form's methods, so if you have name conflicts between a function and a field
	(for example, when you have a field named "fields"), you can use classic function call syntax:
	
		auto formFields = fields(form);
	
	In normal cases, you are allowed to use either classic syntax or uniform function call syntax (UFCS):
	
		auto formFields = form.fields;
 */
class Form
{
}


/**
	Returns a class which contains a Field-based class instance per each field in your form.
	The result is stored in cache, so real instantiating of Field objects will be done only once.
	
	For example, if you have a form like this:
	
		class LoginForm: Form {
			@TextField string text;
			@TextField string password;
		}
	
	then the function fields() will return a class similar to this:
	
		class FormFieldsClass {
			TextField text;
			TextField password;
		}
	
	If you want to do something with the Field-based object and you know its name at compile time,
	it is recommended to use resulting object's members directly.
	This way you will access the exact same Field-based class object as defined without any casts.
	
		TextField passwordField = form.fields.password;
	
	If you don't known name of the field you need at compile-time, then you still can use the result of fields(),
	but you will only get references of the base Field class. To do so, just use the result as an associative array:
	
		Field passwordField = form.fields["password"];
 */
@property auto fields(F)(F form)
if (is(F : Form)) {
	string formFieldsClassDeclaration() {
		string[] fieldDeclarations;
		string[] constructorExpressions;
		string[] mapElement;
		
		foreach (fieldName; FieldNameTuple!F) {
			foreach (uda; __traits(getAttributes, __traits(getMember, F, fieldName))) {
				static if (__traits(compiles, cast(uda) null) && is(uda: Field)) {
					// Line for declaring the field, e.g.: IntegerField id;
					fieldDeclarations ~= uda.classinfo.stringof[8..$-1] ~ " " ~ fieldName ~ ";";
					
					// Line for initializing the Field object, e.g.: id = new IntegerField("id", &form.id);
					constructorExpressions ~= fieldName ~ " = new " ~ uda.classinfo.stringof[8..$-1]
						~ "(\"" ~ fieldName ~ "\", &form."~fieldName~");";
					
					// Line for getting the Field by its name, e.g.: if (fieldName == "id") return id;
					mapElement ~= "\"" ~ fieldName ~ "\": " ~ fieldName ~ ",";
				}
			}
		}
		
		string code =
			"class FormFieldsClass {\n"~
			"  " ~ fieldDeclarations.join("\n  ") ~ "\n" ~
			"  \n" ~
			"  this(F form) {\n" ~
			"    " ~ constructorExpressions.join("\n    ") ~ "\n" ~
			"  }\n" ~
			"  \n" ~
			"  Field opIndex(const string fieldName) {\n" ~
			"    Field[string] map = [\n" ~
			"      " ~ mapElement.join("\n      ") ~ "\n" ~
			"    ];\n" ~
			"    return map.get(fieldName, null);\n" ~
			"  }\n" ~
			"}";
		return code;
	}
	
	mixin(formFieldsClassDeclaration());
	
	static FormFieldsClass[F] cache;
	if (form !in cache)
		cache[form] = new FormFieldsClass(form);
	return cache[form];
}


/**
	Set values of form's fields using a set of key-value pairs.
	
	Each key can be either just a field's name ("password")
	or a field's name with one or more subkeys ("form[userData][password]").
	Depending on that, this will call either field.parseRawIndexedValue() or field.parseRawValue().
 */
void setRawDict(F, D)(F form, D formData)
if (
	is(F : Form)
	&& (is(D == string[string]) || is(D: vibe.inet.webform.FormFields))
) {
	foreach (name, value; formData) {
		size_t indexOfOpeningBracket = name.indexOf('[');
		if (indexOfOpeningBracket > 0 && name[$-1] == ']') {
			string fieldName = name[0..indexOfOpeningBracket];
			string[] keys = name[indexOfOpeningBracket+1..$-1].split("][");
			
			fields(form)[fieldName].parseRawIndexedValue(keys, value);
		}
		else
			fields(form)[name].parseRawValue(value);
	}
}


/**
	Set values of form's fields using a GET string.
	
	Implemented using the setRawDict() function applied to the result of vibe.d's GET parser.
 */
void loadFromGet(F)(F form, string str)
if (is(F: Form)) {
	vibe.inet.webform.FormFields parsedFields;
	vibe.inet.webform.parseURLEncodedForm(str, parsedFields);
	setRawDict(form, parsedFields);
}


/**
	Check all the rules in the form and return true if every one of them returns true, false otherwise.
 */
bool validate(F)(F form)
if (is(F: Form)) {
	// Iterate through members and find those which are form fields.
	foreach (fieldName; FieldNameTuple!F) {
		foreach (uda; __traits(getAttributes, __traits(getMember, F, fieldName))) {
			static if (__traits(compiles, cast(uda) null) && is(uda: Field)) {
				// Iterate through UDAs of the field one more time and find those UDAs which are rules.
				// Let's assume that any function with a Field as first parameter is supposed to be a rule.
				foreach (rule; __traits(getAttributes, __traits(getMember, F, fieldName))) {
					static if (isSomeFunction!rule && is(Parameters!rule[0]: Field)) {
						// Check that the rule has exactly two arguments
						static assert(Parameters!rule.length == 2,
							"Rule " ~ __traits(identifier, rule) ~ "() must have two arguments.");
						
						// Perform the validation of this rule
						bool ruleResult = rule(
							__traits(getMember, form.fields, fieldName),
							__traits(getMember, form, fieldName));
						if (!ruleResult)
							return false;
					}
				}
				
				// Continue to next field in the form
				break;
			}
		}
	}
	
	// If none of the rules returned false, the whole validation returns true
	return true;
}


/**
	Returns true if at least one field was changed since last considerClean() call.
 */
bool isDirty(F)(F form)
if (is(F: Form)) {
	foreach (field; form.fields)
		if (field.dirty)
			return true;
	return false;
}


/**
	Marks all fields to be not dirty, so that later you can see which of them became dirty again.
 */
void considerClean(F)(F form)
if (is(F: Form)) {
	foreach (field; form.fields.tupleof)
		field.dirty = false;
}