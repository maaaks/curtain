module curtain.forms.field;


/**
	An abstract Field. In real life, use BasicField or ArrayField derived from it.
 */
package class Field
{
	/// Field's name which identifies it in the form. Cannot change after initialization.
	immutable string name;
	
	/// Whether the the field was changed after latest form.considerClean() call.
	bool dirty = false;
	
	this(string name) {
		this.name = name;
	}
	
	/**
		The method to be called id "name=value" string is found in GET.
	 */
	abstract void parseRawValue(const string value);
	
	/**
		The method to be called if "name[]=value" or "name[key]=value" or "name[key1][key2]=value" is found in GET.
	 */
	abstract void parseRawIndexedValue(const string[] keys, const string value);
}


/// The simplest rule that makes sure that the field was assigned a value.
bool required(Field field, string value) pure {
	return field.dirty;
}