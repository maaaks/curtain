module curtain.forms.tests.t2_name_collisions;

import curtain.forms;


/**
	This unittest makes sure that every field and all Curtain's functions
	are both accessible even when their names have collisions.
 */
unittest {
	class FieldsCollisionsForm: Form
	{
		@TextField string fields;
		@TextField string setRawDict;
		@TextField string loadFromGet;
	}
	
	auto form = new FieldsCollisionsForm();
	
	assert(typeof(form.fields).stringof == string.stringof);
	assert(typeof(form.setRawDict).stringof == string.stringof);
	assert(typeof(form.loadFromGet).stringof == string.stringof);
	
	assert(typeof(fields(form)).stringof == "FormFieldsClass");
	assert(typeof(setRawDict(form, string[string].init)).stringof == "void");
	assert(typeof(loadFromGet(form, "")).stringof == "void");
}

unittest {
	class MethodsCollisionsForm: Form
	{
		@IntegerField size_t id;
		
		bool fields() {
			return true;
		}
		
		bool setRawDict(string[string] formData) {
			return true;
		}
		
		bool loadFromGet(string str) {
			return true;
		}
	}
	
	auto form = new MethodsCollisionsForm();
	
	assert(form.fields());
	assert(form.setRawDict(string[string].init));
	assert(form.loadFromGet(""));
	
	assert(typeof(fields(form)).stringof == "FormFieldsClass");
	assert(typeof(setRawDict(form, string[string].init)).stringof == "void");
	assert(typeof(loadFromGet(form, "")).stringof == "void");
}