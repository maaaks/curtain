module curtain.forms.tests.t3_rules;

import curtain.forms;


unittest {
	class TestForm : Form
	{
		@TextField
		string field1;
		
		@TextField @required
		string field2;
	}
	auto form = new TestForm;
	
	form.setRawDict(["field1": ""]);
	assert(form.validate() == false);
	form.setRawDict(["field1": "", "field2": ""]);
	assert(form.validate());
	
	form.considerClean();
	assert(form.validate() == false);
}


unittest {
	class TestForm : Form
	{
		@TextField @checkMinLength!2
		string field;
	}
	auto form = new TestForm;
	
	form.setRawDict(["field": ""]);
	assert(form.validate() == false);
	form.setRawDict(["field": "1"]);
	assert(form.validate() == false);
	form.setRawDict(["field": "12"]);
	assert(form.validate());
	form.setRawDict(["field": "123"]);
	assert(form.validate());
}


unittest {
	class TestForm : Form
	{
		@TextField @checkMaxLength!5
		string field;
	}
	auto form = new TestForm;
	
	form.setRawDict(["field": ""]);
	assert(form.validate());
	form.setRawDict(["field": "1"]);
	assert(form.validate());
	form.setRawDict(["field": "12"]);
	assert(form.validate());
	form.setRawDict(["field": "123"]);
	assert(form.validate());
	form.setRawDict(["field": "1234"]);
	assert(form.validate());
	form.setRawDict(["field": "12345"]);
	assert(form.validate());
	form.setRawDict(["field": "123456"]);
	assert(form.validate() == false);
}


unittest {
	class TestForm : Form
	{
		@TextField @checkMinLength!2 @checkMaxLength!5
		string field;
	}
	auto form = new TestForm;
	
	form.setRawDict(["field": ""]);
	assert(form.validate() == false);
	form.setRawDict(["field": "1"]);
	assert(form.validate() == false);
	form.setRawDict(["field": "12"]);
	assert(form.validate());
	form.setRawDict(["field": "123"]);
	assert(form.validate());
	form.setRawDict(["field": "1234"]);
	assert(form.validate());
	form.setRawDict(["field": "12345"]);
	assert(form.validate());
	form.setRawDict(["field": "123456"]);
	assert(form.validate() == false);
}


unittest {
	class TestForm : Form
	{
		@TextField @checkLength!(2, 5)
		string field;
	}
	auto form = new TestForm;
	
	form.setRawDict(["field": ""]);
	assert(form.validate() == false);
	form.setRawDict(["field": "1"]);
	assert(form.validate() == false);
	form.setRawDict(["field": "12"]);
	assert(form.validate());
	form.setRawDict(["field": "123"]);
	assert(form.validate());
	form.setRawDict(["field": "1234"]);
	assert(form.validate());
	form.setRawDict(["field": "12345"]);
	assert(form.validate());
	form.setRawDict(["field": "123456"]);
	assert(form.validate() == false);
}