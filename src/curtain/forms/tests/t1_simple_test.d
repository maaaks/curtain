module curtain.forms.tests.t1_simple_test;

import curtain.forms;
import std.meta;
import std.traits;


/**
	This unittest makes sure that the Form's main functionality works correctly:
	that field declarations are detected correctly and that their values can be assigned.
 */
unittest {
	// Declare a simple form with three Fields
	class TestForm : Form
	{
		@IntegerField
		size_t id;
		
		@TextField
		string name;
		
		@(ArrayField!string)
		string[] aliases;
		
		// This member is not controlled by Form
		bool otherVar;
	}
	auto form = new TestForm;
	
	// Check that form.fields generate a class with one member per each form's field,
	// and that all that members are initialized properly
	static assert(FieldNameTuple!(typeof(form.fields)) == AliasSeq!("id", "name", "aliases"));
	static assert(is(FieldTypeTuple!(typeof(form.fields)) == AliasSeq!(IntegerField, TextField, ArrayField!string)));
	assert(form.fields.id !is null);
	assert(form.fields.name !is null);
	assert(form.fields.aliases !is null);
	
	// Check getting field types by name in runtime
	assert(form.fields["id"].classinfo == typeid(IntegerField));
	assert(form.fields["name"].classinfo == typeid(TextField));
	assert(form.fields["aliases"].classinfo == typeid(ArrayField!string));
	
	// Check how values are assigned using an associative array
	form.setRawDict(["id": "10", "name": "Max", "aliases[0]": "Maxim", "aliases[1]": "Maksim"]);
	assert(form.id == 10);
	assert(form.name == "Max");
	assert(form.aliases == ["Maxim", "Maksim"]);
	
	// Check how fields can be partially rewritten
	form.setRawDict(["id": "20"]);
	assert(form.id == 20);
	assert(form.name == "Max");
	assert(form.aliases == ["Maxim", "Maksim"]);
	
	// Check how values are assigned using a GET string
	form.loadFromGet("id=11&name=Max%20Alibaev&aliases[]=Maks");
	assert(form.id == 11);
	assert(form.name == "Max Alibaev");
	assert(form.aliases == ["Maxim", "Maksim", "Maks"]);
}