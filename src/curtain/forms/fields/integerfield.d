module curtain.forms.fields.integerfield;

import curtain.forms.fields.basicfield;


class IntegerField: BasicField!size_t
{
	mixin generateBasicFieldConstructor;
}


bool checkMinValue(size_t min)(IntegerField field, size_t value) pure {
	return value >= min;
}


bool checkMaxValue(size_t max)(IntegerField field, size_t value) pure {
	return value <= max;
}


bool checkValueRange(size_t min, size_t max)(IntegerField field, size_t value) pure {
	return value >= min && value <= max;
}