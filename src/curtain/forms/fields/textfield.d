module curtain.forms.fields.textfield;

import curtain.forms.fields.basicfield;


class TextField: BasicField!string
{
	mixin generateBasicFieldConstructor;
}


bool checkMinLength(size_t min)(TextField field, string value) pure {
	return value.length >= min;
}


bool checkMaxLength(size_t max)(TextField field, string value) pure {
	return value.length <= max;
}


bool checkLength(size_t min, size_t max)(TextField field, string value) pure {
	return value.length >= min && value.length <= max;
}