module curtain.forms.fields.arrayfield;

import curtain.forms.field;
import std.algorithm.comparison;
import std.conv;


class ArrayField(ValueType): Field
{
	private ValueType[]* arr;
	
	this(string name, ValueType[]* arr) {
		super(name);
		this.arr = arr;
	}
	
	override void parseRawValue(const string value) {
		try {
			*arr ~= convert(value);
			dirty = true;
		}
		catch (ConvException) {
			// When a value is not convertable to ValueType, do nothing
		}
	}
	
	override void parseRawIndexedValue(const string[] keys, const string value) {
		try {
			// This field type supports only single-dimensional arrays
			if (keys.length > 1)
				return;
			
			// When key is not specified, assume that (length+1) was passes
			if (keys.length == 0) {
				(*arr).length += 1;
				(*arr)[$-1] = convert(value);
				dirty = true;
			}
			else {
				// Detect key to place the value under and extend the array to that length if needed
				size_t key = keys[0].to!size_t;
				(*arr).length = max((*arr).length, key+1);
				
				// Assign the value
				(*arr)[key] = convert(value);
				dirty = true;
			}
		}
		catch (ConvException) {
			// When a key is not an integer or a value is not convertable to ValueType, do nothing
		}
	}
	
	ValueType convert(const string value) {
		return value.to!ValueType;
	}
}


unittest {
	int[] arr;
	Field field = new ArrayField!int("arr", &arr);
	assert(field.name == "arr");
	
	// Try assigning non-indexed values
	field.parseRawValue("10");
	field.parseRawIndexedValue([], "20");
	field.parseRawIndexedValue([], "30");
	assert(arr == [10, 20, 30]);
	
	// Try assigning indexed values
	field.parseRawIndexedValue(["1"], "40");
	assert(arr == [10, 40, 30]);
	field.parseRawIndexedValue(["3"], "50");
	assert(arr == [10, 40, 30, 50]);
	field.parseRawIndexedValue(["6"], "60");
	assert(arr == [10, 40, 30, 50, 0, 0, 60]);
	
	// Make sure the field ignores non-integer keys or values
	field.parseRawValue("abc");
	field.parseRawIndexedValue(["1"], "abc");
	field.parseRawIndexedValue(["abc"], "70");
	assert(arr == [10, 40, 30, 50, 0, 0, 60]);
}