module curtain.forms.fields.basicfield;

import curtain.forms.field;
import std.conv;


/**
	A field which only supports parsing one GET parameter, not an array.
 */
class BasicField(ValueType): Field
{
	private ValueType* var;
	
	this(string name, ValueType* var) {
		super(name);
		this.var = var;
	}
	
	override void parseRawValue(const string value) {
		try {
			*var = convert(value);
			dirty = true;
		}
		catch (ConvException) {
			// When a value is not convertable to ValueType, do nothing
		}
	}
	
	override void parseRawIndexedValue(const string[] keys, const string value) {
	}
	
	ValueType convert(const string value) {
		return value.to!ValueType;
	}
}


mixin template generateBasicFieldConstructor()
{
	import std.traits;
	alias ValueType = TemplateArgsOf!(typeof(this))[0];
	
	this(string name, ValueType* var) {
		super(name, var);
	}
}


unittest {
	int var;
	Field field = new BasicField!int("num", &var);
	assert(field.name == "num");
	
	// Try assigning single values
	field.parseRawValue("23");
	field.parseRawValue("45");
	assert(var == 45);
	
	// Make sure the field ignores non-integer values
	field.parseRawValue("abc");
	assert(var == 45);
	
	// Make sure the field ignores indexed values
	field.parseRawIndexedValue(["1"], "67");
	field.parseRawIndexedValue([null], "89");
	assert(var == 45);
}