module curtain.activerecord.hasone;

protected enum _ar_relationHasOne { foo };

/**
	Defines a one-to-one relation between current class and another class (RelClass).
	
	Usage:
	---
	class Entry
	{
		mixin ARId;
		mixin ARHasOne!(ulong, "author_id", User, "author");
		mixin AR;
	}
	---
	
	ActiveRecord generates getters and setters for both KeyType and RelClass,
	so that you can change value either by id or by object.
	---
	entry.author_id = 4;
	assert(entry.author.id == 4);
	
	entry.author = User.find(`id=4`);
	assert(entry.author_id == 4);
	---
 */
mixin template ARHasOne(KeyType, string keyName, RelClass, string relName) {
	mixin _ar_impl_hasOne!(KeyType, keyName, RelClass, relName);
}


/// Please do not use this directly.
mixin template _ar_impl_hasOne(KeyType, string keyName, RelClass, string relName)
{
	import std.conv;
	
	// Field for storing foreign key
	mixin _ar_impl_field!(KeyType, keyName);
	
	// Getter
	mixin ("@_ar_relationHasOne @property RelClass "~relName~"() { return _ar_get!relName(); }");
	RelClass _ar_get(string S)() if (S == relName) {
		if (mixin("isnull_"~keyName~"()"))
			return null;
		else
			return RelClass.find(mixin(keyName)); // ARField will do caching for us
	}
	
	// Setter
	mixin ("@property void "~relName~"(RelClass value) { _ar_set!relName(value); }");
	void _ar_set(string S)(RelClass value) if (S == relName) {
		if (value) {
			mixin("_ar_status_"~keyName) = _ar_fieldstatus.Null;
			mixin(keyName) = __traits(getMember, value, RelClass._ar_idid);
		} else
			mixin("nullify_"~keyName)();
	}
}