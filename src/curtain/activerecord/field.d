module curtain.activerecord.field;

protected enum _ar_field { foo };
protected enum _ar_pseudofield { foo };

protected enum _ar_fieldstatus {
	NotSet, /// Value is not set or loaded at all.
	Set,    /// Value is set.
	Null,   /// Value is set to NULL.
}


/**
	Declares a getter and a setter for a normal ActiveRecord field.
 */
mixin template ARField(FieldType, string fieldName) {
	mixin _ar_impl_field!(FieldType, fieldName);
}


/**
	Declares a field that is the primpary in this table.
	There should be exactly one ARId in each ActiveRecord class.
 */
mixin template ARId(FieldType=ulong, string fieldName="id") {
	mixin _ar_impl_field!(FieldType, fieldName, true);
}


/**
	Declares a pseudofield.
	
	A pseudofield is a field that does not actually exists in the table
	but can be in some cases calculated in the SQL query and easily read into a field in D.
	
	For example, if you perform an aggregate query with column COUNT(*) AS numOfSomething,
	and there is a pseudofield called numOfSomething in your class,
	it will be automatically filled with the value from the column.
	But when calling save(), numOfSomething is, of course, being ignored.
 */
mixin template ARPseudoField(FieldType, string fieldName) {
	mixin _ar_impl_field!(FieldType, fieldName, false, true);
}


/// Please do not use this directly.
mixin template _ar_impl_field(FieldType, string fieldName, bool isId=false, bool pseudo=false)
{
	import std.conv;
	import std.datetime;
	import std.variant;
	
	// Variables to store the field itself and its current status
	mixin("private FieldType _ar_field_"~fieldName~";");
	mixin("private _ar_fieldstatus _ar_status_"~fieldName~";");
	
	// If the field is primary («id»), remember its type and name (_ar_idtype, _ar_idid)
	// and also create a reference to it (_ar_id).
	static if (isId) {
		alias FieldType _ar_idtype;
		static immutable string _ar_idid = fieldName;
		mixin(`alias _ar_field_`~fieldName~` _ar_id;`);
	}
	
	// Getter for humans
	static if (pseudo)
		mixin("@_ar_pseudofield @property FieldType "~fieldName~"() { return _ar_field_"~fieldName~"; }");
	else
		mixin("@_ar_field       @property FieldType "~fieldName~"() { return _ar_field_"~fieldName~"; }");
	
	// Setter for humans
	mixin("@property void "~fieldName~"(FieldType value) { _ar_set!fieldName(value); }");
	private void _ar_set(string S)(FieldType value) if (S == fieldName) {
		// Assign value
		static if (isId) {
			if (value != _ar_id)
				_ar_instances.remove(_ar_id);
		}
		mixin("_ar_field_"~fieldName) = value;
		mixin("_ar_status_"~fieldName) = _ar_fieldstatus.Set;
		static if (isId) {
			_ar_instances[value] = this;
		}
	}
	
	// The setter for robots (with array of bytes)
	mixin("@property void "~fieldName~"(immutable(ubyte)* value) { _ar_set!fieldName(value); }");
	private void _ar_set(string S)(immutable(ubyte)* value) if (S == fieldName) {
		static if (is(FieldType == DateTime)) {
			// Convert value to string
			// as in fromStringz(): http://forum.dlang.org/thread/in2gut$2fss$1@digitalmars.com
			immutable string pgString = (cast(char*)value).to!string;               // "2014-07-17 15:37:41.207533"
			if (pgString.length >= 19) {
				immutable string isoString = pgString[0..10]~"T"~pgString[11..19];  // "2014-07-17T15:37:41"
				mixin("_ar_field_"~fieldName) = DateTime.fromISOExtString(isoString);
				mixin("_ar_status_"~fieldName) = _ar_fieldstatus.Set;
			}
		}
		else static if (is(FieldType == bool)) {
			immutable string str = (cast(char*)value).to!string;
			mixin("_ar_field_"~fieldName) = (str=="t");
			mixin("_ar_status_"~fieldName) = _ar_fieldstatus.Set;
		}
		else {
			immutable string str = (cast(char*)value).to!string;
			mixin("_ar_field_"~fieldName) = str.length>0 ? str.to!FieldType : FieldType.init;
			mixin("_ar_status_"~fieldName) = _ar_fieldstatus.Set;
			static if (isId) {
				_ar_instances[str.to!FieldType] = this;
			}
		}
	}
	
	// Set the value to NULL
	mixin("void nullify_"~fieldName~"() { _ar_status_"~fieldName~" = _ar_fieldstatus.Null; }");
	
	// Check whether the value is NULL
	mixin("bool isnull_"~fieldName~"() { return _ar_status_"~fieldName~"==_ar_fieldstatus.Null; }");
	
	// Unset the value
	mixin("void reset_"~fieldName~"() { _ar_status_"~fieldName~" = _ar_fieldstatus.NotSet; }");
}