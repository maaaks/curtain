module curtain.activerecord.row;

import derelict.pq.pq;
import std.conv;
import std.string;
import std.traits;

struct Row
{
	PGresult* res;
	immutable uint rowNum;
	
	this(T=uint)(PGresult* res, in T rowNum=0) if (isIntegral!T) {
		immutable uint ntuples = PQntuples(res);
		if (ntuples < rowNum + 1)
			throw new OutOfRangeException(ntuples, rowNum);
		
		this.res = res;
		this.rowNum = cast(uint) rowNum;
	}
	
	bool hasColumn(in string name) const {
		immutable int colNum = PQfnumber(res, name.toStringz());
		return colNum != -1;
	}
	
	string[] columnNames() const {
		string[] names;
		foreach (i; 0..PQntuples(res))
			names ~= PQfname(cast(PGresult*)res, i).to!string;
		return names;
	}
	
	immutable(ubyte)* getRaw(in string name) const {
		immutable int colNum = PQfnumber(res, name.toStringz());
		if (colNum == -1) return null;
		return cast(immutable(ubyte)*) PQgetvalue(res, rowNum, colNum);
	}
	
	T get(T)(in string name, lazy T defValue=T.init) const {
		auto value = getRaw(name);
		if (value is null) return defValue;
		return (cast(char*)value).to!string.to!T;
	}
	
	T get(T)(in uint colNum, lazy T defValue=T.init) const {
		if (PQgetisnull(res, rowNum, colNum) == 1) return T.init;
		auto value = PQgetvalue(res, rowNum, colNum);
		if (value is null) return defValue;
		return (cast(char*)value).to!string.to!T;
	}
	
	bool isNull(in string name) const {
		return isNull(PQfnumber(res, name.toStringz()));
	}
	
	bool isNull(in uint colNum) const {
		return (PQgetisnull(res, rowNum, colNum) == 1);
	}
	
	
	class OutOfRangeException : Exception {
		this(in uint ntuples, in ulong rowNum) {
			super("Got only "~ntuples.to!string~" rows, can't get "~rowNum.to!string~".");
		}
	}
}