module curtain.router.router;

import curtain.router.route;
import std.algorithm;
import std.typecons;

class Router(Data)
{
	private Route[] routes;       /// Stores routes, ordered
	private bool areRoutesSorted; /// Is false when list has changed since last sorting
	private Data[Route] datas;    /// Stored data, indexed by routes
	
	/**
		Add a new route.
	 */
	void addRoute(in string templateString, Data data) {
		addRoute(new Route(templateString), data);
	}
	
	/**
		Add a new route.
	 */
	void addRoute(Route route, Data data) {
		routes ~= route;
		areRoutesSorted = false;
		datas[route] = data;
	}
	
	/**
		Get route that matches given URL.
	 */
	Route.MatchResult getRoute(in string url) {
		// Sort routes if not sorted
		if (!areRoutesSorted) {
			sort(routes);
			areRoutesSorted = true;
		}
		
		// Iterate through routes until one will match the URL
		foreach (route; routes) {
			auto result = route.match(url);
			if (result.status != Route.MatchStatus.DoesNotMatch)
				return result;
		}
		
		// No route matched it? Then return null
		return null;
	}
	
	/**
		Get data associated with given route.
	 */
	Data getData(Route route) {
		return datas.get(route, null);
	}
}