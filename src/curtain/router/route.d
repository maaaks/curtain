module curtain.router.route;

import std.algorithm;
import std.array;
import std.range;
import std.regex;
import std.string;

/**
	A very simple parser of strings like "[name].ongra.net/[id]".
	Converts it into regex: "^([^/]+)\.ongra\.net/([^/]+)$" and array of parameter names: ["name", "id"].
	Parameters can't contain slashes.
	
	The template can be used for both rendeding URLs using given parameters and for matching URLs.
	When matching an URL, Route also checks whether it can be matched if append/remove trailing slash.
 */
class Route
{
	/**
		The status component of MatchResult.
	 */
	enum MatchStatus {
		DoesNotMatch,
		MatchesPerfectly,
		NeedToAppendSlash,
		NeedToRemoveSlash,
	}
	
	/**
		Result of matching a URL with a Route.
		Contains information about how exact did the URL matched (if matched)
		and which strings were parsed as parameters.
	 */
	class MatchResult
	{
		Route route;
		MatchStatus status;
		string[string] params;
	}
	
	/**
		Internal information about a position for a single parameter.
		These objects are stored in router.placeholder, indexed by placeholders' names.
	 */
	private struct Placeholder {
		size_t begin;
		size_t end;
		bool isDigitsOnly;
		
		this(in size_t begin, in size_t end, in bool isDigitsOnly) {
			this.begin = begin;
			this.end = end;
			this.isDigitsOnly = isDigitsOnly;
		}
	}
	
	
	immutable string templateString;  /// The whole original template string
	immutable string domainString;    /// The domain part of original template string
	immutable string pathString;      /// The path part of original template string
	Regex!char domainRegex;           /// Regexp to match the domain part of URL
	Regex!char pathRegex;             /// Regexp to match the path part of URL
	Placeholder[string] placeholders; /// Begin and end positions of every placeholder
	
	/**
		Generate regexps and remember placeholders' positions based on the original string.
	 */
	this(in string templateString) {
		this.templateString = templateString;
		
		// Split given URL into two parts: the domain and the path
		auto parts = separateDomainAndPath(templateString);
		this.domainString = parts[0];
		this.pathString   = parts[1];
		
		// Prepare regex for detecting group delimiters in the strings
		auto rx = ctRegex!(
			`  \[ [^\]]+ \]  ` ~  // Any symbols, like in [login].ongra.net
			`| \( [^\)]+ \)  ` ~  // Digits only, like in [login].ongra.net/(id)
			`| \.            ` ~  // A dot (we need to escape it for regex)
			`| [^\[\(\.]+    `    // Hardcoded part of URL (not inside any brackets)
		, "x");
		
		// Prepare replacer — a function that will generate templateRegexString
		// and remember all named groups and their types in the isDigitsOnly array
		string replacer(Captures!string m) {
			if (m.hit == ".") {
				return `\.`;
			}
			else if (m.hit[0] == '[' && m.hit[$-1] == ']') {
				auto name = m.hit[1..$-1];
				auto begin = templateString.indexOf(m.hit);
				placeholders[name] = Placeholder(begin, begin+m.hit.length, false);
				return `(?P<`~name~`>[^/]+)`;
			}
			else if (m.hit[0] == '(' && m.hit[$-1] == ')') {
				auto name = m.hit[1..$-1];
				auto begin = templateString.indexOf(m.hit);
				placeholders[name] = Placeholder(begin, begin+m.hit.length, true);
				return `(?P<`~name~`>\d+)`;
			}
			else {
				return m.hit;
			}
		}
		
		// Perform the replacements on both parts and save resulting regexps
		this.domainRegex = regex(`^` ~ replaceAll!replacer(domainString, rx) ~ `$`);
		this.pathRegex   = regex(`^` ~ replaceAll!replacer(pathString,   rx) ~ `$`);
	}
	
	/**
		Render the Route using given parameters instead of corresponding placeholders.
	 */
	string render(string[string] params=string[string].init) const {
		string url = templateString;
		ptrdiff_t delta = 0;
		
		foreach (name, ph; placeholders) {
			// Get the value from the params passed to the function
			immutable string value = params.get(name, null);
			if (!value)
				throw new Exception("No parameter \""~name~"\" given.");
			
			// Since length of previously inserted values may differ from length of "["+name+"]" or "("+name+")",
			// we add or substract the difference from the position where we're going to make new substitution.
			size_t begin = ph.begin + delta;
			size_t end   = ph.end + delta;
			url = url[0..begin] ~ value ~ url[end..$];
			
			// And, of course, change the delta for correct future substitutions
			delta += value.length - name.length - 2;
		}
		
		return url;
	}
	
	/**
		Check whether the URL (given as domain and path separate parts) matches the Route.
	 */
	MatchResult match(in string domain, in string path) {
		RegexMatch!string domainCaps;
		RegexMatch!string pathCaps;
		MatchResult result = new MatchResult;
		
		// First, check the domain
		domainCaps = domain.match(domainRegex);
		if (!domainCaps) {
			result.status = MatchStatus.DoesNotMatch;
			goto finalize;
		}
		
		pathCaps = path.match(pathRegex);
		if (pathCaps) {
			result.status = MatchStatus.MatchesPerfectly;
			goto finalize;
		}
		
		if (path.length && path[$-1] == '/') {
			pathCaps = path[0..$-1].match(pathRegex);
			if (pathCaps) {
				result.status = MatchStatus.NeedToRemoveSlash;
				goto finalize;
			}
		}
		
		pathCaps = (path~'/').match(pathRegex);
		if (pathCaps) {
			result.status = MatchStatus.NeedToAppendSlash;
			goto finalize;
		}
		
finalize:
		// Construct the MatchResult and return it
		if (result.status != MatchStatus.DoesNotMatch) {
			// Extract the params from both domainCaps and pathCaps
			foreach (name; placeholders.keys())
				try { result.params[name] = domainCaps.captures[name]; }
				catch(Exception e) {
					try { result.params[name] = pathCaps.captures[name]; }
					catch(Exception e) {}
				}
		}
		result.route = this;
		return result;
	}
	
	/**
		Check whether the URL matches the Route.
	 */
	MatchResult match(in string url) {
		auto parts = separateDomainAndPath(url);
		return match(parts[0], parts[1]);
	}
	
	/**
		Little aux function to convert strings like "example.org/bla/bla" into ["example.org", "bla/bla"].
	 */
	private static string[2] separateDomainAndPath(in string input) {
		auto slashPos = input.indexOf('/');
		return slashPos!=-1
			? [ input[0..slashPos], input[slashPos+1..$] ]
			: [ input, "" ];
	}
	
	alias opCmp = Object.opCmp;
	int opCmp(Route that) {
		int cmp(size_t a, size_t b) {
			if (a < b)
				return -1;
			else if (a > b)
				return 1;
			else
				return 0;
		}
		
		// First, compare the domains. Less parametrized domain goes first.
		Placeholder[] thisDomainParams = this.placeholders.values.filter!(p => p.end < domainString.length).array();
		Placeholder[] thatDomainParams = that.placeholders.values.filter!(p => p.end < domainString.length).array();
		if (thisDomainParams.length != thatDomainParams.length)
			return cmp(thisDomainParams.length, thatDomainParams.length);
		
		// If domains have the same number of parameters, compare how much of them are digits-only.
		// More specified domain (the one with more digits-only placeholders) goes first.
		auto thisDomainDigitsOnlyCount = thisDomainParams.filter!(p => p.isDigitsOnly).array().length;
		auto thatDomainDigitsOnlyCount = thatDomainParams.filter!(p => p.isDigitsOnly).array().length;
		if (thisDomainDigitsOnlyCount != thatDomainDigitsOnlyCount)
			return 0 - cmp(thisDomainDigitsOnlyCount, thatDomainDigitsOnlyCount);
		
		// Else, compare paths. Less parametrized path goes first.
		Placeholder[] thisPathParams = this.placeholders.values.filter!(p => p.end >= domainString.length).array();
		Placeholder[] thatPathParams = that.placeholders.values.filter!(p => p.end >= domainString.length).array();
		if (thisPathParams.length != thatPathParams.length)
			return cmp(thisPathParams.length, thatPathParams.length);
		
		// If paths have the same number of parameters, compare how much of them are digits-only.
		// More specified path (the one with more digits-only placeholders) goes first.
		auto thisPathDigitsOnlyCount = thisPathParams.filter!(p => p.isDigitsOnly).array().length;
		auto thatPathDigitsOnlyCount = thatPathParams.filter!(p => p.isDigitsOnly).array().length;
		if (thisPathDigitsOnlyCount != thatPathDigitsOnlyCount)
			return 0 - cmp(thisPathDigitsOnlyCount, thatPathDigitsOnlyCount);
		
		// Else, assume that the Routes are almost equal.
		return 0;
	}
}


unittest {
	auto rBlog = new Route("[name].ongra.net/");
	auto rPost = new Route("[name].ongra.net/(id)");
	auto rHelp = new Route("help.ongra.net/[page]");
	
	// Check how Route constructs url
	assert(rBlog.render(["name":"greatperson"]) == "greatperson.ongra.net/");
	assert(rPost.render(["name":"greatperson", "id":"13769"]) == "greatperson.ongra.net/13769");
	assert(rHelp.render(["page":"blogs"]) == "help.ongra.net/blogs");
	
	// Check how Route parses URLs
	with (rBlog.match("greatperson.ongra.net/")) {
		assert(status == Route.MatchStatus.MatchesPerfectly);
		assert(params["name"] == "greatperson");
	}
	
	with (rBlog.match("greatperson.ongra.net")) {
		assert(status == Route.MatchStatus.MatchesPerfectly);
		assert(params["name"] == "greatperson");
	}
	
	with (rBlog.match("greatperson.ongra.net/13769")) {
		assert(status == Route.MatchStatus.DoesNotMatch);
	}
	
	with (rPost.match("greatperson.ongra.net/13769")) {
		assert(status == Route.MatchStatus.MatchesPerfectly);
		assert(params["name"] == "greatperson");
		assert(params["id"] == "13769");
	}
	
	with (rPost.match("greatperson.ongra.net/13769/")) {
		assert(status == Route.MatchStatus.NeedToRemoveSlash);
		assert(params["name"] == "greatperson");
		assert(params["id"] == "13769");
	}
	
	with (rPost.match("greatperson.ongra.net/")) {
		assert(status == Route.MatchStatus.DoesNotMatch);
	}
	
	with (rPost.match("greatperson.ongra.net/abcde")) {
		assert(status == Route.MatchStatus.DoesNotMatch);
	}
	
	with (rHelp.match("help.ongra.net/blogs")) {
		assert(status == Route.MatchStatus.MatchesPerfectly);
	}
}


unittest {
	// Test routes sorting
	assert(new Route("exact.example.org/bar") < new Route("[any].example.org/bar"));
	assert(new Route("example.org/(digits)")  < new Route("example.org/[letters]"));
	
	// Test routes sorting, backwards
	assert(new Route("[any].example.org/bar") > new Route("exact.example.org/bar"));
	assert(new Route("example.org/[letters]") > new Route("example.org/(digits)"));
}